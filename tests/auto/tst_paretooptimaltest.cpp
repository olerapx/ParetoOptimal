#include <QtTest>
#include <QCoreApplication>
#include <QList>

#include <paretofinder.h>

class ParetoOptimalTest : public QObject
{
    Q_OBJECT

public:
    ParetoOptimalTest();
    ~ParetoOptimalTest();

private:
    ParetoFinder pareto;

private slots:
    void findTest();

    void normalizeTest();
    void compareTest();
};

ParetoOptimalTest::ParetoOptimalTest()
{

}

ParetoOptimalTest::~ParetoOptimalTest()
{

}

void ParetoOptimalTest::findTest()
{
    QList<CriteriaDirection> directions({CriteriaDirection::MAX, CriteriaDirection::MAX, CriteriaDirection::MAX});

    QList<Alternative> firstWorse({Alternative({10, 11, 12}), Alternative({12, 13, 14}), Alternative({13, 14, 15})});

    pareto.find(firstWorse, directions);

    QVERIFY(firstWorse.size() == 1);
    QVERIFY(firstWorse[0].getPoints() == QList<int>({13, 14, 15}));

    QList<Alternative> firstBetter({Alternative({13, 14, 15}), Alternative({12, 13, 14}), Alternative({10, 11, 12})});

    pareto.find(firstBetter, directions);

    QVERIFY(firstBetter.size() == 1);
    QVERIFY(firstBetter[0].getPoints() == QList<int>({13, 14, 15}));

    QList<Alternative> incomparable({Alternative({1, 5, 10}), Alternative({3, 3, 15})});

    pareto.find(incomparable, directions);
    QVERIFY(incomparable.size() == 2);

    QList<Alternative> empty;
    pareto.find(empty, directions);

    QList<Alternative> single({Alternative({2, 3, 1})});
    pareto.find(single, directions);

    QVERIFY(single.size() == 1);
}

void ParetoOptimalTest::normalizeTest()
{
    Alternative a1({10, 0, -10, 5, -5});
    Alternative a2({4, 100, 9, 75, 46});

    QList<Alternative> alternatives = {a1, a2};
    QList<CriteriaDirection> directions = {CriteriaDirection::MAX, CriteriaDirection::MIN, CriteriaDirection::MIN, CriteriaDirection::MIN, CriteriaDirection::MAX};

    pareto.normalize(alternatives, directions);

    QVERIFY(alternatives[0].getPoints() == QList<int>({10, 0, 10, -5, -5}));
    QVERIFY(alternatives[1].getPoints() == QList<int>({4, -100, -9, -75, 46}));
}

void ParetoOptimalTest::compareTest()
{
    Alternative a1({1, 2, 3, 4});

    Alternative better({2, 3, 4, 5});

    QVERIFY(pareto.compare(a1, better) == CompareResult::WORSE);

    Alternative betterEqual({2, 3, 3, 5});
    QVERIFY(pareto.compare(a1, betterEqual) == CompareResult::WORSE);

    Alternative incomparable({2, 1, 4, 5});
    QVERIFY(pareto.compare(a1, incomparable) == CompareResult::INCOMPARABLE);

    Alternative equalWorse({1, 1, 1, 1});
    QVERIFY(pareto.compare(a1, equalWorse) == CompareResult::BETTER);

    Alternative equalBetter({1, 10, 10, 10});
    QVERIFY(pareto.compare(a1, equalBetter) == CompareResult::WORSE);

    Alternative equal({1, 2, 3, 4});
    QVERIFY(pareto.compare(a1, equal) == CompareResult::EQUAL);

    Alternative single1({1}), single2({2});
    QVERIFY(pareto.compare(single1, single2) == CompareResult::WORSE);

    Alternative empty1, empty2;
    QVERIFY(pareto.compare(empty1, empty2) == CompareResult::BETTER);
}

QTEST_MAIN(ParetoOptimalTest)

#include "tst_paretooptimaltest.moc"
