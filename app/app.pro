#-------------------------------------------------
#
# Project created by QtCreator 2017-04-29T15:30:34
#
#-------------------------------------------------

TARGET = ParetoOptimal
TEMPLATE = app

QT += gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += $$PWD

include (../src/src.pri)
LIBS += -L../src -lparetooptimal

HEADERS  += gui/mainwindow.h \

SOURCES += main.cpp\
        gui/mainwindow.cpp \

FORMS    += gui/mainwindow.ui
