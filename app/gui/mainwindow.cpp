#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->tableWidget->setItemDelegate(new Delegate());

    addTableContextMenu();

    addDirectionRow();

    connect(&pareto, ParetoFinder::sendDeleteAlternative, this, &MainWindow::onDeleteAlternative);

    for(int i=0; i<3; i++)
        addCriteria();

    for(int i=0; i<5; i++)
        addAlternative();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addTableContextMenu()
{
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect (ui->tableWidget, &QTableWidget::customContextMenuRequested, this, &MainWindow::contextMenuEvent);
}

void MainWindow::onDeleteAlternative(int index)
{
    ui->tableWidget->removeRow(index + 1);
}

void MainWindow::contextMenuEvent(const QPoint &pos)
{
    QMenu menu(this);

    QAction *removeCriteria = menu.addAction(tr("Удалить критерий"));
    QAction *removeAlternative = menu.addAction(tr("Удалить альтернативу"));

    QAction *a = menu.exec(ui->tableWidget->viewport()->mapToGlobal(pos));

    if (a == removeCriteria)
    {
        int col = ui->tableWidget->itemAt(pos)->column();
        ui->tableWidget->removeColumn(col);
    }
    else if (a == removeAlternative)
    {
        int row = ui->tableWidget->itemAt(pos)->row();
        ui->tableWidget->removeRow(row);
    }
}

void MainWindow::addDirectionRow()
{
    addRow("");
}

void MainWindow::addRow(QString title)
{
    ui->tableWidget->setRowCount(ui->tableWidget->rowCount() + 1);
    ui->tableWidget->setVerticalHeaderItem(ui->tableWidget->rowCount()-1, new QTableWidgetItem(title));
}

void MainWindow::addCriteria()
{
    addColumn(QString("К%1").arg(lastCriteriaNumber+1));
    lastCriteriaNumber ++;

    int col = ui->tableWidget->columnCount() - 1;
    addDirectionBox(col);

    for(int i=1; i<ui->tableWidget->rowCount(); i++)
        ui->tableWidget->setItem(i, col, new QTableWidgetItem("0"));
}

void MainWindow::addColumn(QString title)
{
     ui->tableWidget->setColumnCount(ui->tableWidget->columnCount()+1);
     ui->tableWidget->setHorizontalHeaderItem(ui->tableWidget->columnCount() - 1, new QTableWidgetItem(title));
}

void MainWindow::addDirectionBox(int col)
{
    ui->tableWidget->setCellWidget(0, col, createDirectionBox());
}

QComboBox* MainWindow::createDirectionBox()
{
    QComboBox* box = new QComboBox();
    box->addItem("max");
    box->addItem("min");
    box->setCurrentIndex(0);

    return box;
}

void MainWindow::addAlternative()
{
    addRow(QString("А%1").arg(lastAlternativeNumber+1));
    lastAlternativeNumber ++;

    int row = ui->tableWidget->rowCount() - 1;

    for(int i=0; i< ui->tableWidget->columnCount(); i++)
        ui->tableWidget->setItem(row, i, new QTableWidgetItem("0"));
}

void MainWindow::on_addCriteriaButton_clicked()
{
    addCriteria();
}

void MainWindow::on_addAlternativeButton_clicked()
{
    addAlternative();
}

void MainWindow::on_findParetoButton_clicked()
{
    QList<Alternative> alternatives;
    QList<CriteriaDirection> directions;

    for(int i=0; i<ui->tableWidget->columnCount(); i++)
    {
        QComboBox* box = static_cast<QComboBox*>(ui->tableWidget->cellWidget(0, i));

        int index = box->currentIndex();
        if (index == 0)
            directions.append(CriteriaDirection::MAX);
        else
            directions.append(CriteriaDirection::MIN);
    }

    for (int i=1; i<ui->tableWidget->rowCount(); i++)
    {
        Alternative a;

        for(int j=0; j<ui->tableWidget->columnCount(); j++)
        {
            a.getPoints().append(ui->tableWidget->item(i, j)->text().toInt());
        }

        alternatives.append(a);
    }

    try
    {
      pareto.find(alternatives, directions);
    }
    catch (Exception e)
    {
        QMessageBox::critical(this, "Error", e.what());
    }
}

void MainWindow::on_randomFillButton_clicked()
{
    std::mt19937 mt(rd());

    std::uniform_int_distribution<> headerDist(0, 1);
    std::uniform_int_distribution<> cellDist(-100, 100);

    for(int i=0; i<ui->tableWidget->columnCount(); i++)
    {
        int direction = headerDist(mt);
        static_cast<QComboBox*>(ui->tableWidget->cellWidget(0, i))->setCurrentIndex(direction);
    }

    for (int i=1; i<ui->tableWidget->rowCount(); i++)
    {
        for(int j=0; j<ui->tableWidget->columnCount(); j++)
        {
           ui->tableWidget->item(i, j)->setText(QString::number(cellDist(mt)));
        }
    }
}
