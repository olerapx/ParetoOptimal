#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <QLineEdit>
#include <QMessageBox>
#include <QMenu>
#include <random>

#include "util/delegate.h"
#include "paretofinder.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_addCriteriaButton_clicked();
    void on_addAlternativeButton_clicked();

    void onDeleteAlternative(int index);

    void on_findParetoButton_clicked();

    void on_randomFillButton_clicked();

private:
    Ui::MainWindow *ui;
    ParetoFinder pareto;

    std::random_device rd;

    void addTableContextMenu();
    void contextMenuEvent(const QPoint& pos);

    int lastCriteriaNumber = 0;
    int lastAlternativeNumber = 0;

    void addDirectionRow();
    void addRow(QString title);

    void addCriteria();
    void addColumn(QString title);

    void addDirectionBox(int col);
    QComboBox* createDirectionBox();

    void addAlternative();
};

#endif // MAINWINDOW_H
