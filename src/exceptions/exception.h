#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <QException>
#include <QString>

class Exception: public QException
{
protected:
    QString error;

public:
    Exception(QString what);
    QString what() { return this->error; }

    void raise() const { throw *this; }
    Exception *clone() const { return new Exception(*this); }
};

#endif // EXCEPTION_H
