TARGET = paretooptimal
TEMPLATE = lib

INCLUDEPATH += $$PWD
include (src.pri)

SOURCES += \
    models/alternative.cpp \
    paretofinder.cpp \
    exceptions/exception.cpp
