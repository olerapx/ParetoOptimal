#ifndef DELEGATE_H
#define DELEGATE_H

#include <QItemDelegate>
#include <QLineEdit>

class Delegate : public QItemDelegate
{
public:
    QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
    {
        QLineEdit* lineEdit = new QLineEdit(parent);

        QIntValidator* validator = new QIntValidator(lineEdit);
        lineEdit->setValidator(validator);
        lineEdit->setAlignment(Qt::AlignCenter);
        return lineEdit;
    }
};

#endif // DELEGATE_H
