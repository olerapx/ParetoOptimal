CONFIG -= debug_and_release debug_and_release_target
QT       += core

INCLUDEPATH += $$PWD

HEADERS  += \
    $$PWD/models/alternative.h \
    $$PWD/util/delegate.h \
    $$PWD/models/criteriadirection.h \
    $$PWD/paretofinder.h \
    $$PWD/exceptions/exception.h
