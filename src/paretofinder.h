#ifndef PARETOFINDER_H
#define PARETOFINDER_H

#include <QList>

#include "models/alternative.h"
#include "models/criteriadirection.h"
#include "exceptions/exception.h"

enum class CompareResult
{
    BETTER,
    WORSE,
    EQUAL,
    INCOMPARABLE
};

class ParetoFinder: public QObject
{
    Q_OBJECT

public:
    ParetoFinder();

    void find(QList<Alternative>& alternatives, QList<CriteriaDirection>& directions);
    void normalize(QList<Alternative>& alternatives, QList<CriteriaDirection>& directions);

    CompareResult compare (Alternative a1, Alternative a2);

signals:
    void sendDeleteAlternative(int index);
};

#endif // PARETOFINDER_H
