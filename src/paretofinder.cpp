#include "paretofinder.h"

ParetoFinder::ParetoFinder()
{

}

void ParetoFinder::find(QList<Alternative>& alternatives, QList<CriteriaDirection>& directions)
{
    normalize(alternatives, directions);

    if (alternatives.size() < 2)
        return;

    for(int i=0; i<alternatives.size(); i++)
    {
        for(int j=i+1; j<alternatives.size(); j++)
        {
            CompareResult res = compare(alternatives[i], alternatives[j]);

            if (res == CompareResult::BETTER || res == CompareResult::EQUAL)
            {
                alternatives.removeAt(j);
                sendDeleteAlternative(j);

                j--;
            }
            else if (res == CompareResult::WORSE)
            {
                alternatives.removeAt(i);
                sendDeleteAlternative(i);

                i--;
                break;
            }
        }
    }
}

void ParetoFinder::normalize(QList<Alternative> &alternatives, QList<CriteriaDirection> &directions)
{
    for (int j=0; j<directions.size(); j++)
    {
        if(directions[j] == CriteriaDirection::MAX)
            continue;

        for(int i=0; i<alternatives.size(); i++)
            alternatives[i].getPoints()[j] = -alternatives[i].getPoints()[j];
    }
}

CompareResult ParetoFinder::compare(Alternative a1, Alternative a2)
{
    if (a1.getPoints().size() != a2.getPoints().size())
        throw Exception("Alternatives must have the same size");

    if (a1.getPoints().size() == 0)
        return CompareResult::BETTER;

    CompareResult res;

    if (a1.getPoints()[0] > a2.getPoints()[0])
        res = CompareResult::BETTER;
    else if (a1.getPoints()[0] < a2.getPoints()[0])
        res = CompareResult::WORSE;
    else res = CompareResult::EQUAL;

    for(int i=1; i<a1.getPoints().size(); i++)
    {
        if (res == CompareResult::BETTER && a1.getPoints()[i] < a2.getPoints()[i])
            return CompareResult::INCOMPARABLE;

        if (res == CompareResult::WORSE && a1.getPoints()[i] > a2.getPoints()[i])
            return CompareResult::INCOMPARABLE;

        if (res == CompareResult::EQUAL)
        {
            if (a1.getPoints()[i] > a2.getPoints()[i])
                res = CompareResult::BETTER;
            else if (a1.getPoints()[i] < a2.getPoints()[i])
                res = CompareResult::WORSE;
        }
    }

    return res;
}
