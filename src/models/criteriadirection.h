#ifndef CRITERIADIRECTION_H
#define CRITERIADIRECTION_H

#include <QList>

enum class CriteriaDirection
{
    MAX,
    MIN
};

#endif // CRITERIADIRECTION_H
