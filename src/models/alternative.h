#ifndef ALTERNATIVE_H
#define ALTERNATIVE_H

#include <QList>

class Alternative
{
public:
    Alternative();
    Alternative(QList<int> points);

    QList<int>& getPoints() { return points; }

private:
    QList<int> points;
};

#endif // ALTERNATIVE_H
